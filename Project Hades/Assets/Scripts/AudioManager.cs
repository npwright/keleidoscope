﻿using UnityEngine;
using System;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    #region Variables

    [SerializeField]
    public Sound[] sounds;

    public static AudioManager instance;

    #endregion

    #region Main Menu
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.playOnAwake = s.playOnAwake;
            if (s.soundEffect == true)
            {
               // s.source.volume = Drill.UI.Drill_UI_Manager.soundVolume * Drill.UI.Drill_UI_Manager.masterVolume;
            }
            else if (s.musicEffect == true)
            {
                // s.source.volume = Drill.UI.Drill_UI_Manager.musicVolume * Drill.UI.Drill_UI_Manager.masterVolume;
            }
        }
    }

    public void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
    }

    public void OnLevelWasLoaded(int level) // Checks when new scene is loaded to check if to play music
    {
        if (level == 0)
        {
            Play("Background_Music");
        }
    }

    public void Update()
    {
        foreach (Sound s in sounds)
        {
            if (s.soundEffect == true)
            {
              //  s.source.volume = Drill.UI.Drill_UI_Manager.soundVolume * Drill.UI.Drill_UI_Manager.masterVolume;
            }
            else if (s.musicEffect == true)
            {
                //s.source.volume = Drill.UI.Drill_UI_Manager.musicVolume * Drill.UI.Drill_UI_Manager.masterVolume;
            }
        }
    }

    public void Play(string name) // Plays requested sound when called 
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.Log("Sound " + name + " does not exist!");
            return;
        }

        s.source.Play();
    }

    #endregion
}
