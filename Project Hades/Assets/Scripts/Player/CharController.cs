﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class CharController : MonoBehaviour
    {
        #region Variables

        [SerializeField]
        float moveSpeed = 4f;

        Vector3 forward, right;
        public Animator animator;
        public float InputX;
        public float InputY;
        public float InputZ;
        public float movement;

        #endregion

        void Start()
        {
            forward = Camera.main.transform.forward;
            forward.y = 0;
            forward = Vector3.Normalize(forward);
            right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
            animator = this.gameObject.GetComponent<Animator>();
        }

        void Update()
        {
            if (Input.anyKey)
            {
                Move();
            }

            InputY = Input.GetAxis("Vertical");
            animator.SetFloat("InputY", InputY);

            InputX = Input.GetAxis("Horizontal");
            animator.SetFloat("InputX", InputX);

            if(InputY > InputX && InputY != 0)
            {
                movement = InputY;
            } else if(InputX > InputY && InputX != 0)
            {
                movement = InputX;
            } else
            {
                movement = Mathf.Abs(InputY + InputX);
            }

            animator.SetFloat("Movement", movement);
        }

        void Move()
        {
            Vector3 direction = new Vector3(Input.GetAxis("HorizontalKey"), 0, Input.GetAxis("VerticalKey"));
            Vector3 rightMovement = right * moveSpeed * Time.deltaTime * Input.GetAxis("HorizontalKey");
            Vector3 upMovement = forward * moveSpeed * Time.deltaTime * Input.GetAxis("VerticalKey");

            Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

            transform.forward = heading;
            transform.position += rightMovement;
            transform.position += upMovement;
        }
    }
}
