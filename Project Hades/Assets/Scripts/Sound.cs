﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{
    #region Variables

    public string name;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;
    [Range(0.1f, 3f)]
    public float pitch;

    public bool loop;
    public bool playOnAwake;

    [Header("Sound Type")]
    public bool musicEffect;
    public bool soundEffect;


    [HideInInspector]
    public AudioSource source;

    #endregion

    #region Main Methods
    #endregion

    #region Helper Methods

    public void SetSource(AudioSource _source) // Sends clip to AudioSource
    {
        source = _source;
        source.clip = clip;
    }

    public void Play() // Plays Sound
    {
        source.Play();
    }

    #endregion
}
