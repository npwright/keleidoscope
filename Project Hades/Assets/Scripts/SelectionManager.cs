﻿using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    [SerializeField]
    private string selectableTag = "Selectable";

    private Transform _selection;

    void Update()
    {

        if (_selection != null)
        {
            var selectionOutline = _selection.GetComponent<Outline>();
            selectionOutline.GetComponent<Outline>().OnDisable();
            _selection = null;
        }


        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit))
        {
            var selection = hit.transform;
            if (selection.CompareTag(selectableTag))
            {
                var selectionOutline = selection.GetComponent<Outline>();
                if (selectionOutline != null)
                {
                    selectionOutline.GetComponent<Outline>().OnEnable();
                }
                _selection = selection;
            }
        }
    }

}
