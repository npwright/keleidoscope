﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Object : MonoBehaviour, IClickable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnLeftClick()
    {

    }

    public void OnRightClick()
    {
        if (transform.parent.GetComponent<Outline>().enabled)
        {
            transform.parent.GetComponent<Outline>().enabled = false;
        }
        else
        {
            transform.parent.GetComponent<Outline>().enabled = true;
        }
    }

    public void OnHoverEnter()
    {
        transform.parent.GetComponent<Outline>().enabled = true;
    }

    public void OnHoverExit()
    {
        transform.parent.GetComponent<Outline>().enabled = false;
    }
}
