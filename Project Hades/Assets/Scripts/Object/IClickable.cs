﻿public interface IClickable
{
    void OnLeftClick();
    void OnRightClick();

    void OnHoverEnter();
    void OnHoverExit();
}
