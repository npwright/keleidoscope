﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon : BaseWeapon  
{

    public Weapon (string itemName, string itemDescription, int itemID){
        ItemName = itemName;
        ItemDescription = itemDescription;
       // ItemType = ItemType;
        //WeaponType = weaponType;
        ItemID = itemID;
    }
    
}
