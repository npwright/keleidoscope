﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;


public class WeaponDatabaseEditor : EditorWindow
{
    private enum State{
        BLANK,
        EDIT,
        ADD
    }

    private State state;
    private int selectedWeapon;
    private string newItemName;
    [TextArea(3,8)]
    private string newItemDescription;
    //public Weapon newWeaponType;
    private int newItemID;

    private const string DATABASE_PATH = @"Assets/Resources/weaponDB.asset";

    private WeaponDatabase weapons;
    private Vector2 _scrollPos;

    public string NewItemDescription { get => newItemDescription; set => newItemDescription = value; }

    [MenuItem("Items/Weapons/Weapon Database %#w")]
    public static void Init(){
        WeaponDatabaseEditor window = EditorWindow.GetWindow<WeaponDatabaseEditor>();
        window.minSize = new Vector2 (800,400);
        window.Show();
    }

    void OnEnable(){
        if(weapons == null){
            LoadDatabase();
        }

        state = State.BLANK;
    }

    void OnGUI(){
        EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth(true));
        DisplayListArea();
        DisplayMainArea();
        EditorGUILayout.EndHorizontal();
    }

    void LoadDatabase(){
        weapons = (WeaponDatabase)AssetDatabase.LoadAssetAtPath(DATABASE_PATH, typeof(WeaponDatabase));
        if(weapons == null){
            CreateDatabase();
        }
    }

    void CreateDatabase(){
        weapons = ScriptableObject.CreateInstance<WeaponDatabase>();
        AssetDatabase.CreateAsset(weapons, DATABASE_PATH);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    void DisplayListArea(){
        EditorGUILayout.BeginVertical(GUILayout.Width (250));
        EditorGUILayout.Space();

        _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, "box", GUILayout.ExpandHeight(true));

        for(int i = 0; i < weapons.COUNT; i++){
            EditorGUILayout.BeginHorizontal();
            if(GUILayout.Button ("-", GUILayout.Width(25))){
                weapons.RemoveAt(i);
                weapons.SortByItemID();
                EditorUtility.SetDirty(weapons);
                state = State.BLANK;
                return;
            }

            if(GUILayout.Button (weapons.Weapon (i).ItemName, "box", GUILayout.ExpandWidth(true))){
                selectedWeapon = i;
                state = State.EDIT;
            }

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        EditorGUILayout.LabelField("Weapons: " + weapons.COUNT, GUILayout.Width(100));

        if(GUILayout.Button("New Weapon")){
            state = State.ADD;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }
    void DisplayMainArea(){
        EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
        EditorGUILayout.Space();

        switch(state){
            case State.ADD:
                DisplayAddMainArea();
                break;
            case State.EDIT:
                DisplayEditMainArea();
                break;
            default:
                DisplayBlankMainArea();
                break;
        }

        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }

    void DisplayBlankMainArea(){
        EditorGUILayout.LabelField(
            "There are 3 things that can be displayed here.\n" +
            "1) Weapon info for editing\n" +
            "2) Black fields for adding a new weapon\n" +
            "3) Blank Area",
            GUILayout.ExpandHeight(true));
    }

    void DisplayEditMainArea(){
        weapons.Weapon(selectedWeapon).ItemName = EditorGUILayout.TextField(new GUIContent("Name: "), weapons.Weapon (selectedWeapon).ItemName);
        weapons.Weapon(selectedWeapon).ItemDescription = EditorGUILayout.TextField(new GUIContent("Description: "), weapons.Weapon (selectedWeapon).ItemDescription);
        //weapons.Weapon(selectedWeapon).WeaponType = (Weapon.WeaponTypes)(EditorGUILayout.EnumPopup(new GUIContent("Weapon Type: "), weapons.Weapon (selectedWeapon).WeaponType));
        weapons.Weapon(selectedWeapon).ItemID = int.Parse(EditorGUILayout.TextField(new GUIContent("Item ID: "), weapons.Weapon (selectedWeapon).ItemID.ToString()));

        EditorGUILayout.Space();

        if(GUILayout.Button("Done", GUILayout.Width(100))){
            weapons.SortByItemID();
            EditorUtility.SetDirty(weapons);
            state = State.BLANK;
        }
    }

    void DisplayAddMainArea(){
        newItemName = EditorGUILayout.TextField(new GUIContent("Name: "), newItemName);
        newItemDescription = EditorGUILayout.TextField(new GUIContent("Description: "), newItemDescription);
        newItemID = Convert.ToInt32(EditorGUILayout.TextField (new GUIContent ("Item ID: "), newItemID.ToString()));

        EditorGUILayout.Space();

        if(GUILayout.Button("Done", GUILayout.Width(100))){
            weapons.Add(new Weapon(newItemName, newItemDescription, newItemID));
            weapons.SortByItemID();

            newItemName = string.Empty;
            newItemDescription = string.Empty;
            newItemID = 0;
            EditorUtility.SetDirty(weapons);
            state = State.BLANK;
        }
    }
}
