﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponDatabase : ScriptableObject
{

    [SerializeField]
    private List<Weapon> database;

    void OnEnable(){
        if(database == null){
            database = new List<Weapon>();
        }
    }

    public void Add(Weapon weapon){
        database.Add(weapon);
    }

    public void Remove(Weapon weapon){
        database.Remove(weapon);
    }

    public void RemoveAt(int i){
        database.RemoveAt(i);
    }

    public int COUNT{
        get{return database.Count;}
    }

    public Weapon Weapon (int i){
        return database.ElementAt(i);
    }

    public void SortByItemID(){
        //database.Sort((x,y) => x.ItemName.CompareTo(y.ItemName));
        //database.Sort ((x, y) => string.Compare (x.ItemName, y.ItemName));
        database.Sort(SortByID);
        
        
    }

    static int SortByID(Weapon weapon1, Weapon weapon2){
        return weapon2.ItemID.CompareTo(weapon1.ItemID);
        
    }

}
