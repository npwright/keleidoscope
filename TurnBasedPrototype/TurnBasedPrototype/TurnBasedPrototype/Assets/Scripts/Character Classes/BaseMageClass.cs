﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMageClass : BaseCharacterClass
{

    public BaseMageClass()
    {
        CharacterName = "Mage";
        CharacterDescription = "A smart and wise hero.";
        Strength = 4;
        Perception = 5;
        Endurance = 4;
        Charisma = 7;
        Intelligence = 10;
        Agility = 6;
        Luck = 4;

    }

}
