﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWarriorClass : BaseCharacterClass
{

    public BaseWarriorClass()
    {
        CharacterName = "Max Stone";
        CharacterDescription = "'Stone' to his friends, Maxwell is the largest person currently living in Vault 13. He is known for his physical strength and stamina. He would make the ideal volunteer due to his tremendous size and strength. It is unfortunate that his intelligence was affected after birth when the labor bot dropped him on his head. He doesn't care that he might have to leave the vault.'";
        Sex = "Male";
        Age = 23;
        Level = 1;
        Strength = 8;
        Perception = 4;
        Endurance = 9;
        Charisma = 4;
        Intelligence = 4;
        Agility = 7;
        Luck = 4;
        CurrentXP = 0;
        SkillPoints = 0;
        RequiredXP = 1000;

        

        //Traits
        BloodyMess = false;
        Bruiser = true;
        ChemReliant = false;
        ChemResistant = false;
        FastMetabolism = false;
        FastShot = false;
        Finesse = false;
        Gifted = false;
        GoodNatured = false;
        HeavyHanded = true;
        Jinxed = false;
        Kamikaze = false;
        OneHander = false;
        SexAppeal = false;
        Skilled = false;
        SmallFrame = false;

        //Skill Tags
        SmallGunsTag = true;
        BigGunsTag = true;
        EnergyWeaponsTag = false;
        UnarmedTag = true;
        MeleeWeaponsTag = false;
        ThrowingTag = false;
        FirstAidTag = false;
        DoctorTag = false;
        SneakTag = false;
        LockpickTag = false;
        StealTag = false;
        TrapsTag = false;
        ScienceTag = false;
        RepairTag = false;
        SpeechTag = false; 
        BarterTag = false;
        GamblingTag = false;
        OutdoorsmanTag = false;


    }

}

