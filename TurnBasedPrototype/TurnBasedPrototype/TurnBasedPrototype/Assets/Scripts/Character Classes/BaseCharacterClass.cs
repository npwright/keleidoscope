﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacterClass
{

    private string characterName;
    private string characterDescription;
    private string sex;
    private int age;
    private int currentXP;
    private int requiredXP;
    private int skillPoints;
    private int level;

    //Primary Stats
    private int strength;
    private int perception;
    private int endurance;
    private int charisma;
    private int intelligence;
    private int agility;
    private int luck;

    //Traits
    private bool bloodyMess;
    private bool bruiser;
    private bool chemReliant;
    private bool chemResistant;
    private bool fastMetabolism;
    private bool fastShot;
    private bool finesse;
    private bool gifted;
    private bool goodNatured;
    private bool heavyHanded;
    private bool jinxed;
    private bool kamikaze;
    private bool oneHander;
    private bool sexAppeal;
    private bool skilled;
    private bool smallFrame;

    //Skills
        //Combat skills
    private int smallGuns;
    private int bigGuns;
    private int energyWeapons;
    private int unarmed;
    private int meleeWeapons;
    private int throwing;
        //Active skills
    private int firstAid;
    private int doctor;
    private int sneak;
    private int lockpick;
    private int steal;
    private int traps;
    private int science;
    private int repair;
        //Passive skills
    private int speech;
    private int barter;
    private int gambling;
    private int outdoorsman;

    //Skill Tags
        //Combat skills
    private bool smallGunsTag;
    private bool bigGunsTag;
    private bool energyWeaponsTag;
    private bool unarmedTag;
    private bool meleeWeaponsTag;
    private bool throwingTag;
        //Active skills
    private bool firstAidTag;
    private bool doctorTag;
    private bool sneakTag;
    private bool lockpickTag;
    private bool stealTag;
    private bool trapsTag;
    private bool scienceTag;
    private bool repairTag;
        //Passive skills
    private bool speechTag;
    private bool barterTag;
    private bool gamblingTag;
    private bool outdoorsmanTag;


    public string CharacterName{get;set;}
    public string CharacterDescription{get;set;}
    public string Sex { get; set; }
    public int Age { get; set; }
    public int CurrentXP { get; set;}
    public int RequiredXP { get; set; }
    public int SkillPoints {get;set;}
    public int Level{get;set;}

    public int Strength { get; set; }
    public int Perception { get; set; }
    public int Endurance { get; set; }
    public int Charisma { get; set; }
    public int Intelligence { get; set; }
    public int Agility { get; set; }
    public int Luck { get; set; }

        //Getters and Setters Traits
    public bool BloodyMess{get;set;}
    public bool Bruiser{get;set;}
    public bool ChemReliant{get;set;}
    public bool ChemResistant{get;set;}
    public bool FastMetabolism{get;set;}
    public bool FastShot{get;set;}
    public bool Finesse{get;set;}
    public bool Gifted{get;set;}
    public bool GoodNatured{get;set;}
    public bool HeavyHanded{get;set;}
    public bool Jinxed{get;set;}
    public bool Kamikaze{get;set;}
    public bool OneHander{get;set;}
    public bool SexAppeal{get;set;}
    public bool Skilled{get;set;}
    public bool SmallFrame{get;set;}

        //Getters and Setters Skills
    public int SmallGuns{get;set;}
    public int BigGuns{get;set;}
    public int EnergyWeapons{get;set;}
    public int Unarmed{get;set;}
    public int MeleeWeapons{get;set;}
    public int Throwing{get;set;}
    public int FirstAid{get;set;}
    public int Doctor{get;set;}
    public int Sneak{get;set;}
    public int Lockpick{get;set;}
    public int Steal{get;set;}
    public int Traps{get;set;}
    public int Science{get;set;}
    public int Repair{get;set;}
    public int Speech{get;set;}
    public int Barter{get;set;}
    public int Gambling{get;set;}
    public int Outdoorsman{get;set;}

        //Getters and Setters Skill Tags
    public bool SmallGunsTag{get;set;}
    public bool BigGunsTag{get;set;}
    public bool EnergyWeaponsTag{get;set;}
    public bool UnarmedTag{get;set;}
    public bool MeleeWeaponsTag{get;set;}
    public bool ThrowingTag{get;set;}
    public bool FirstAidTag{get;set;}
    public bool DoctorTag{get;set;}
    public bool SneakTag{get;set;}
    public bool LockpickTag{get;set;}
    public bool StealTag{get;set;}
    public bool TrapsTag{get;set;}
    public bool ScienceTag{get;set;}
    public bool RepairTag{get;set;}
    public bool SpeechTag{get;set;}
    public bool BarterTag{get;set;}
    public bool GamblingTag{get;set;}
    public bool OutdoorsmanTag{get;set;}
}
