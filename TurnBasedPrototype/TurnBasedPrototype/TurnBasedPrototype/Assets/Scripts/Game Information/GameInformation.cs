﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInformation : MonoBehaviour
{

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public static string PlayerName { get; set; }
    public static BaseCharacterClass PlayerClass { get; set; }
    public static int PlayerLevel { get; set; }
    public static int Strength { get; set; }
    public static int Perception { get; set; }
    public static int Endurance { get; set; }
    public static int Charisma { get; set; }
    public static int Intellect { get; set; }
    public static int Agility { get; set; }
    public static int Luck { get; set; }
    public static BaseWeapon WeaponOne { get; set; }
    public static int CurrentXP { get; set; }
    public static int RequiredXP { get; set; }

}
