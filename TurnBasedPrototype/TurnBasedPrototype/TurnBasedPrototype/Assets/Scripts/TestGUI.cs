﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGUI : MonoBehaviour
{

    private BaseCharacterClass class1 = new BaseMageClass();
    private BaseCharacterClass class2 = new BaseWarriorClass();

    private void OnGUI()
    {
        GUILayout.Label(class1.CharacterName);
        GUILayout.Label(class1.CharacterDescription);
        GUILayout.Label(class1.Strength.ToString());
        GUILayout.Label(class1.Perception.ToString());
        GUILayout.Label(class1.Endurance.ToString());
        GUILayout.Label(class1.Charisma.ToString());
        GUILayout.Label(class1.Intelligence.ToString());
        GUILayout.Label(class1.Agility.ToString());
        GUILayout.Label(class1.Luck.ToString());

        GUILayout.Label(class2.CharacterName);
        GUILayout.Label(class2.CharacterDescription);
        GUILayout.Label(class2.Strength.ToString());
        GUILayout.Label(class2.Perception.ToString());
        GUILayout.Label(class2.Endurance.ToString());
        GUILayout.Label(class2.Charisma.ToString());
        GUILayout.Label(class2.Intelligence.ToString());
        GUILayout.Label(class2.Agility.ToString());
        GUILayout.Label(class2.Luck.ToString());
    }
}
