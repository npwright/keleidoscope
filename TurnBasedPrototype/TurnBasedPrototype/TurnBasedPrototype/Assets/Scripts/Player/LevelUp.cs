﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp {

    public int maxPlayerLevel = 99;

    public void LevelUpCharacter()
    {
        if(GameInformation.CurrentXP > GameInformation.RequiredXP)
        {
            if(GameInformation.PlayerLevel < maxPlayerLevel)
            {
                GameInformation.PlayerLevel += 1;
            }
            else
            {
                GameInformation.PlayerLevel = maxPlayerLevel;
            }
        GameInformation.RequiredXP = DetermineRequiredXP(GameInformation.PlayerLevel);
        }

    }

    private int DetermineRequiredXP(int playerLevel)
    {
        return (playerLevel * (playerLevel - 1) / 2) * 1000;
    }

    public int NextLevelReturn(int playerlevel){
        return DetermineRequiredXP(playerlevel);
    }


}
