﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float moveSpeed = 25.0f;
    public float rotateSpeed = 5.0f;
    public float sprintSpeed = 50.0f;

    
    void Update()
    {
        if (Input.GetKey("w"))
        {
            transform.Translate((Vector3.forward) * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey("s"))
        {
            transform.Translate((Vector3.back) * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey("a"))
        {
            transform.Translate((Vector3.left) * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey("d"))
        {
            transform.Translate((Vector3.right) * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey("q"))
        {
            transform.Rotate((Vector3.up) * rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("left shift"))
        {
            moveSpeed = sprintSpeed;
        }

    }
}
