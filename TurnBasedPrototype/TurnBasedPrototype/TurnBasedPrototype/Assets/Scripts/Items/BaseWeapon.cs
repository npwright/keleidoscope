﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class BaseWeapon
{

    private string itemName;
    private string itemDescription;
    private int itemID;

    public enum WeaponTypes
    {
        SMALLGUN,
        BIGGUN,
        ENERGEYWEAPON,
        UNARMORED,
        MELEEWEAPON,
        THROWING
    }

    public enum AttackTypes{
        SINGLE,
        AIMED,
        SWING,
        THRUST,
        THROW,
        BURST,
        NONE
    }

    public enum DamageTypes{
        NORMAL,
        LASER,
        FIRE,
        PLASMA,
        EXPLODE,
        EMP,
        ELECTRICAL,
        NONE
    }

    public enum ItemTypes
    {
        AMMUNITION,
        ARMOR,
        CONSUMABLE,
        HOLODISK,
        BOOK,
        WEAPON,
        OTHER
    }

    private WeaponTypes weaponType;
    private AttackTypes attackType1;
    private AttackTypes attackType2;
    private AttackTypes attackType3;
    private DamageTypes damageType;
    private int effectID;
    private int minDMG;
    private int maxDMG;
    private int minSTR;
    private int ammunition1;
    private int ammunition2;
    private int handsReq;
    private int magazineSize;
    private int value;
    private int range;
    private int weight;


    public string ItemName{get;set;}
    public string ItemDescription{get;set;}
    public int ItemID{get;set;}
    public ItemTypes ItemType{get;set;}
    public WeaponTypes WeaponType{get;set;}
    public AttackTypes AttackType1{get;set;}
    public AttackTypes AttackType2{get;set;}
    public AttackTypes AttackType3{get;set;}
    public DamageTypes DamageType{get;set;}
    public int EffectID{get;set;}
    public int MinDMG{get;set;}
    public int MaxDMG{get;set;}
    public int MinSTR{get;set;}
    public int Ammunition1{get;set;}
    public int Ammunition2{get;set;}
    public int HandsReq{get;set;}
    public int MagazineSize{get;set;}
    public int Value{get;set;}
    public int Range{get;set;}
    public int Weight{get;set;}

}
