﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewPotion : MonoBehaviour
{

    private BasePotion newPotion;

    private void Start()
    {
        CreatePotion();
        Debug.Log(newPotion.ItemName);
        Debug.Log(newPotion.ItemDescription);
        Debug.Log(newPotion.ItemID.ToString());
        Debug.Log(newPotion.PotionType);
    }


    private void CreatePotion()
    {
        newPotion = new BasePotion();
        newPotion.ItemName = "Potion";
        newPotion.ItemDescription = "This is a potion";
        newPotion.ItemID = Random.Range(1, 101);

        ChoosePotionType();

        newPotion.SpellEffectID = Random.Range(1, 101);
    }

    private void ChoosePotionType()
    {
        int randomTemp = Random.Range(1, 10);

        switch (randomTemp)
        {
            case 1:
                newPotion.PotionType = BasePotion.PotionTypes.HEALTH;
                break;
            case 2:
                newPotion.PotionType = BasePotion.PotionTypes.ENERGY;
                break;
            case 3:
                newPotion.PotionType = BasePotion.PotionTypes.STRENGTH;
                break;
            case 4:
                newPotion.PotionType = BasePotion.PotionTypes.PERCEPTION;
                break;
            case 5:
                newPotion.PotionType = BasePotion.PotionTypes.ENDURANCE;
                break;
            case 6:
                newPotion.PotionType = BasePotion.PotionTypes.CHARISMA;
                break;
            case 7:
                newPotion.PotionType = BasePotion.PotionTypes.INTELLECT;
                break;
            case 8:
                newPotion.PotionType = BasePotion.PotionTypes.AGILITY;
                break;
            case 9:
                newPotion.PotionType = BasePotion.PotionTypes.SPEED;
                break;
        }
    }
}
