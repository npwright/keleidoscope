﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewEquipment : MonoBehaviour
{

    private BaseEquipment newEquipment;
    private string[] itemNames = new string[4]
    {
    "Common", "Great", "Amazing", "Insane"
    };
    private string[] itemDes = new string[2] { "A new cool item", "A new not so cool item" };

    private void Start()
    {
        CreateEquipment();
        Debug.Log(newEquipment.ItemName);
        Debug.Log(newEquipment.ItemDescription);
        Debug.Log(newEquipment.ItemID.ToString());
        Debug.Log(newEquipment.EquipmentType);
        Debug.Log(newEquipment.Strength.ToString());
        Debug.Log(newEquipment.Perception.ToString());
        Debug.Log(newEquipment.Endurance.ToString());
        Debug.Log(newEquipment.Charisma.ToString());
        Debug.Log(newEquipment.Intelligence.ToString());
        Debug.Log(newEquipment.Agility.ToString());
        Debug.Log(newEquipment.Luck.ToString());

    }

    private void CreateEquipment()
    {
        newEquipment = new BaseEquipment();

        newEquipment.ItemName = itemNames[Random.Range(1, 4)] + " Item";

        newEquipment.ItemDescription = itemDes[Random.Range(0, itemDes.Length)];

        newEquipment.ItemID = Random.Range(1, 101);

        newEquipment.Strength = Random.Range(1, 11);
        newEquipment.Perception = Random.Range(1, 11);
        newEquipment.Endurance = Random.Range(1, 11);
        newEquipment.Charisma = Random.Range(1, 11);
        newEquipment.Intelligence = Random.Range(1, 11);
        newEquipment.Agility = Random.Range(1, 11);
        newEquipment.Luck = Random.Range(1, 11);

        ChooseEquipmentType();

        newEquipment.SpellEffectID = Random.Range(1, 101);
    }

    private void ChooseEquipmentType()
    {
        int randomTemp = Random.Range(1, 9);
        switch (randomTemp)
        {
            case 1:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.HEAD;
                break;
            case 2:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.CHEST;
                break;
            case 3:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.SHOULDERS;
                break;
            case 4:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.LEGS;
                break;
            case 5:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.FEET;
                break;
            case 6:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.NECK;
                break;
            case 7:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.EARS;
                break;
            case 8:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.RINGS;
                break;
            case 9:
                newEquipment.EquipmentType = BaseEquipment.EquipmentTypes.HANDS;
                break;
        }
    }

}
