﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewWeapon : MonoBehaviour
{
    private BaseWeapon newWeapon;

    private void Start()
    {
        CreateWeapon();
        Debug.Log(newWeapon.ItemName);
        Debug.Log(newWeapon.ItemDescription);
        Debug.Log(newWeapon.ItemID.ToString());
        Debug.Log(newWeapon.WeaponType);

    }

    public void CreateWeapon()
    {
        newWeapon = new BaseWeapon();

        newWeapon.ItemName = "W" + Random.Range(1, 101);

        newWeapon.ItemDescription = "This is a new weapon.";

        newWeapon.ItemID = Random.Range(1, 101);


        ChooseWeaponType();

        newWeapon.EffectID = Random.Range(1, 101);

    }

    private void ChooseWeaponType()
    {
        int randomTemp = Random.Range(1, 7);
        switch (randomTemp){
            case 1:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.SMALLGUN;
                break;
            case 2:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.BIGGUN;
                break;
            case 3:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.ENERGEYWEAPON;
                break;
            case 4:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.UNARMORED;
                break;
            case 5:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.MELEEWEAPON;
                break;
            case 6:
                newWeapon.WeaponType = BaseWeapon.WeaponTypes.THROWING;
                break;
        }
    }
}
