﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    void Start()
    {
        LoadInformation.LoadAllInformation();

        Debug.Log("Player Name: " + GameInformation.PlayerName);
        //Debug.Log("Player Class: " + GameInformation.PlayerClass.CharacterClassName);
        Debug.Log("Player Level: " + GameInformation.PlayerLevel);
        Debug.Log("Player Strength: " + GameInformation.Strength);
        Debug.Log("Player Perception: " + GameInformation.Perception);
        Debug.Log("Player Endurance: " + GameInformation.Endurance);
        Debug.Log("Player Charisma: " + GameInformation.Charisma);
        Debug.Log("Player Intellect: " + GameInformation.Intellect);
        Debug.Log("Player Agility: " + GameInformation.Agility);
        Debug.Log("Player Luck: " + GameInformation.Luck);
    }

}
