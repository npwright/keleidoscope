﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreateNewCharacter : MonoBehaviour
{
    private BasePlayer newPlayer;
    private bool isMageClass;
    private bool isWarriorClass;
    private string playerName = "Enter Name";

    void Start()
    {
        newPlayer = new BasePlayer();
    }

    void Update()
    {
        
    }

    private void StoreNewPlayerInfo()
    {
        GameInformation.PlayerName = newPlayer.PlayerName;
        GameInformation.PlayerLevel = newPlayer.PlayerLevel;
        GameInformation.Strength = newPlayer.Strength;
        GameInformation.Perception = newPlayer.Perception;
        GameInformation.Endurance = newPlayer.Endurance;
        GameInformation.Charisma = newPlayer.Charisma;
        GameInformation.Intellect = newPlayer.Intelligence;
        GameInformation.Agility = newPlayer.Agility;
        GameInformation.Luck = newPlayer.Luck;
    }

    private void OnGUI()
    { 
        isMageClass = GUILayout.Toggle(isMageClass, "Mage Class");
        isWarriorClass = GUILayout.Toggle(isWarriorClass, "Max Stone");
        if (GUILayout.Button("Create"))
        {
            if (isMageClass)
            {
                newPlayer.PlayerClass = new BaseMageClass();
            }else if (isWarriorClass)
            {
                newPlayer.PlayerClass = new BaseWarriorClass();
            }
            CreateNewPlayer();
            StoreNewPlayerInfo();
            SaveInformation.SaveAllInformation();

            Debug.Log("Player Name: " + newPlayer.PlayerClass.CharacterName);
            Debug.Log("Player Level: " + newPlayer.PlayerLevel);
            Debug.Log("Player Strength: " + newPlayer.Strength);
            Debug.Log("Player Perception: " + newPlayer.Perception);
            Debug.Log("Player Endurance: " + newPlayer.Endurance);
            Debug.Log("Player Charisma: " + newPlayer.Charisma);
            Debug.Log("Player Intellect: " + newPlayer.Intelligence);
            Debug.Log("Player Agility: " + newPlayer.Agility);
            Debug.Log("Player Luck: " + newPlayer.Luck);
        }
        if (GUILayout.Button("LOAD"))
        {
            SceneManager.LoadScene("BattleScene");
        }
    }

    private void CreateNewPlayer(){
        newPlayer.PlayerLevel = 1;
        newPlayer.Strength = newPlayer.PlayerClass.Strength;
        newPlayer.Perception = newPlayer.PlayerClass.Perception;
        newPlayer.Endurance = newPlayer.PlayerClass.Endurance;
        newPlayer.Charisma = newPlayer.PlayerClass.Charisma;
        newPlayer.Intelligence = newPlayer.PlayerClass.Intelligence;
        newPlayer.Agility = newPlayer.PlayerClass.Agility;
        newPlayer.Luck = newPlayer.PlayerClass.Luck;
        newPlayer.PlayerName = playerName;

        #region Derived Stats
        newPlayer.ActionPoints = 5 + Mathf.FloorToInt((newPlayer.Agility / 2));
        newPlayer.AC = 19 - newPlayer.Agility;
        newPlayer.CarryWeight = 25 + (newPlayer.Strength * 25);
        newPlayer.CriticalChance = newPlayer.Luck * 1;
        newPlayer.DamageResistance = 0;
        newPlayer.HealingRate = Mathf.Max(newPlayer.Endurance / 3 , 1);
        newPlayer.HitPoints = 15 + newPlayer.Strength + (2 * newPlayer.Endurance);
        newPlayer.MeleeDamage = Mathf.Max(newPlayer.Strength - 5, 1);
        newPlayer.PartyLimit = Mathf.FloorToInt(newPlayer.Charisma / 2);
        newPlayer.PerkRate = 3;
        newPlayer.PoisonResistance = newPlayer.Endurance * 5;
        newPlayer.RadiationResistance = newPlayer.Endurance * 2;
        newPlayer.Sequence = newPlayer.Perception * 2;
        newPlayer.SkillRate = (newPlayer.Intelligence * 2) + 5;
        #endregion

        #region Traits
        if(newPlayer.BloodyMess){
            Debug.Log("BloodyMess: on");
        }
        if(newPlayer.Bruiser){
            newPlayer.Strength += 2;
            newPlayer.ActionPoints -= 2;
        }
        if(newPlayer.ChemReliant){
            Debug.Log("ChemReliant: on");
        }
        if(newPlayer.ChemResistant){
            Debug.Log("ChemResistant: on");
        }
        if(newPlayer.FastMetabolism){
            newPlayer.HealingRate += 2;
            newPlayer.PoisonResistance = 0;
            newPlayer.RadiationResistance = 0;
        }
        if(newPlayer.FastShot){
            Debug.Log("FastShot: on");
        }
        if(newPlayer.Finesse){
            newPlayer.CriticalChance += 10;
            Debug.Log("Finesse: on");
        }
        if(newPlayer.Gifted){
            newPlayer.Strength += 1;
            newPlayer.Perception += 1;
            newPlayer.Endurance += 1;
            newPlayer.Charisma += 1;
            newPlayer.Intelligence += 1;
            newPlayer.Agility += 1;
            newPlayer.Luck += 1;
            newPlayer.SmallGuns -= 10;
            newPlayer.BigGuns -= 10;
            newPlayer.EnergyWeapons -= 10;
            newPlayer.Unarmed -= 10;
            newPlayer.MeleeWeapons -= 10;
            newPlayer.Throwing -= 10;
            newPlayer.FirstAid -= 10;
            newPlayer.Doctor -= 10;
            newPlayer.Sneak -= 10;
            newPlayer.Lockpick -= 10;
            newPlayer.Steal -= 10;
            newPlayer.Traps -= 10;
            newPlayer.Science -= 10;
            newPlayer.Repair -= 10;
            newPlayer.Speech -= 10;
            newPlayer.Barter -= 10;
            newPlayer.Gambling -= 10;
            newPlayer.Outdoorsman -= 10;
            
        }
        if(newPlayer.GoodNatured){
            newPlayer.SmallGuns -= 10;
            newPlayer.BigGuns -= 10;
            newPlayer.EnergyWeapons -= 10;
            newPlayer.Unarmed -= 10;
            newPlayer.MeleeWeapons -= 10;
            newPlayer.Throwing -= 10;
            newPlayer.FirstAid += 15;
            newPlayer.Doctor += 15;
            newPlayer.Speech += 15;
            newPlayer.Barter += 15;
        }
        if(newPlayer.HeavyHanded){
            newPlayer.MeleeDamage += 4;
            Debug.Log("HeavyHanded: on");
        }
        if(newPlayer.Jinxed){
            Debug.Log("Jinxed: on");
        }
        if(newPlayer.Kamikaze){
            newPlayer.Sequence += 5;
            newPlayer.AC = 0;
        }
        if(newPlayer.OneHander){
            Debug.Log("OneHander: on");
        }
        if(newPlayer.SexAppeal){
            Debug.Log("SexAppeal: on");
        }
        if(newPlayer.Skilled){
            newPlayer.SkillRate += 5;
            newPlayer.PerkRate = 4;
        }
        if(newPlayer.SmallFrame){
            newPlayer.Agility += 1;
            newPlayer.CarryWeight = 25 + (newPlayer.Strength * 15);
        }
        #endregion

        #region Skills
        newPlayer.SmallGuns = 5 + (4 * newPlayer.Agility);
        newPlayer.BigGuns = 2 * newPlayer.Agility;
        newPlayer.EnergyWeapons = 2 * newPlayer.Agility;
        newPlayer.Unarmed = (30 + (newPlayer.Agility + newPlayer.Strength)) * 2;
        newPlayer.MeleeWeapons = (20 + (newPlayer.Agility + newPlayer.Strength)) * 2;
        newPlayer.Throwing = 4 * newPlayer.Agility;
        newPlayer.FirstAid = 2 * (newPlayer.Perception + newPlayer.Intelligence);
        newPlayer.Doctor = 5 + (newPlayer.Perception + newPlayer.Intelligence);
        newPlayer.Sneak = 5 + (newPlayer.Agility * 3);
        newPlayer.Lockpick = 10 + (newPlayer.Perception + newPlayer.Agility);
        newPlayer.Steal = 3 * newPlayer.Agility;
        newPlayer.Traps = 10 + (newPlayer.Perception + newPlayer.Agility);
        newPlayer.Science = 10 + (2 * newPlayer.Intelligence);
        newPlayer.Repair = 3 * newPlayer.Intelligence;
        newPlayer.Speech = 5 * newPlayer.Charisma;
        newPlayer.Barter = 4 * newPlayer.Charisma;
        newPlayer.Gambling = 5 * newPlayer.Luck;
        newPlayer.Outdoorsman = 2 * (newPlayer.Endurance + newPlayer.Intelligence);
        #endregion

        #region Skill Tags
        if(newPlayer.SmallGunsTag){
            newPlayer.SmallGuns += 20;
        }
        if(newPlayer.BigGunsTag){
            newPlayer.BigGuns += 20;
        }
        if(newPlayer.EnergyWeaponsTag){
            newPlayer.EnergyWeapons += 20;
        }
        if(newPlayer.UnarmedTag){
            newPlayer.Unarmed += 20;
        }
        if(newPlayer.MeleeWeaponsTag){
            newPlayer.MeleeWeapons += 20;
        }
        if(newPlayer.ThrowingTag){
            newPlayer.Throwing += 20;
        }
        if(newPlayer.FirstAidTag){
            newPlayer.FirstAid += 20;
        }
        if(newPlayer.DoctorTag){
            newPlayer.Doctor += 20;
        }
        if(newPlayer.SneakTag){
            newPlayer.Sneak += 20;
        }
        if(newPlayer.LockpickTag){
            newPlayer.Lockpick += 20;
        }
        if(newPlayer.StealTag){
            newPlayer.Steal += 20;
        }
        if(newPlayer.TrapsTag){
            newPlayer.Traps += 20;
        }
        if(newPlayer.ScienceTag){
            newPlayer.Science += 20;
        }
        if(newPlayer.RepairTag){
            newPlayer.Repair += 20;
        }
        if(newPlayer.SpeechTag){
            newPlayer.Speech += 20;
        }
        if(newPlayer.BarterTag){
            newPlayer.Barter += 20;
        }
        if(newPlayer.GamblingTag){
            newPlayer.Gambling += 20;
        }
        if(newPlayer.OutdoorsmanTag){
            newPlayer.Outdoorsman += 20;
        }

        #endregion
        
        newPlayer.Currency = 10;
    }
}
