﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadInformation
{

    public static void LoadAllInformation()
    {
        GameInformation.PlayerName = PlayerPrefs.GetString("PLAYERNAME");
        GameInformation.PlayerLevel =  PlayerPrefs.GetInt("PLAYERLEVEL");
        GameInformation.Strength = PlayerPrefs.GetInt("STRENGTH");
        GameInformation.Perception = PlayerPrefs.GetInt("PERCEPTION");
        GameInformation.Endurance = PlayerPrefs.GetInt("ENDURANCE");
        GameInformation.Charisma = PlayerPrefs.GetInt("CHARISMA");
        GameInformation.Intellect = PlayerPrefs.GetInt("INTELLECT");
        GameInformation.Agility = PlayerPrefs.GetInt("AGILITY");
        GameInformation.Luck = PlayerPrefs.GetInt("LUCK");

        if(PlayerPrefs.GetString("WEAPON1") != null)
        {
            GameInformation.WeaponOne = (BaseWeapon)PPSerialization.Load("WEAPON1");
        }
    }
}
