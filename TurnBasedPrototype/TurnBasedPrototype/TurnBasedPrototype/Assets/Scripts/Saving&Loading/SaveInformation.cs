﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveInformation
{ 
    public static void SaveAllInformation()
    {
        PlayerPrefs.SetString("PLAYERNAME", GameInformation.PlayerName);
        PlayerPrefs.SetInt("PLAYERLEVEL",GameInformation.PlayerLevel);
        PlayerPrefs.SetInt("STRENGTH", GameInformation.Strength);
        PlayerPrefs.SetInt("PERCEPTION", GameInformation.Perception);
        PlayerPrefs.SetInt("ENDURANCE", GameInformation.Endurance);
        PlayerPrefs.SetInt("CHARISMA", GameInformation.Charisma);
        PlayerPrefs.SetInt("INTELLECT", GameInformation.Intellect);
        PlayerPrefs.SetInt("AGILITY", GameInformation.Agility);
        PlayerPrefs.SetInt("LUCK", GameInformation.Luck);

        if(GameInformation.WeaponOne != null)
        {
            PPSerialization.Save("EQUIPMENTITEM1", GameInformation.WeaponOne);
        }

        Debug.Log("SAVED SUCCESSFULLY");
    }

}
